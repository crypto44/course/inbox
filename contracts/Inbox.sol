pragma solidity ^0.8.11;

contract Inbox {
    string public message;

    constructor(string memory initalMessage) {
        message = initalMessage;
    }

    function setMessage(string memory newMessage) public {
        message = newMessage;
    }
}
