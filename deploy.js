const path = require("path");
const fs = require("fs");
const HDWalletProvider = require("@truffle/hdwallet-provider");
const Web3 = require("web3");
const mnemonic = fs.readFileSync("./.mnemonic.json", "utf8");
const mnemonicPhrase = JSON.parse(mnemonic).phrase;
const provider = new HDWalletProvider({
  mnemonic: {
    phrase: mnemonicPhrase,
  },
  providerOrUrl:
    "https://rinkeby.infura.io/v3/7ee63a5f00ea43e8a3d077b91d2ffd74",
});
const web3 = new Web3(provider);
const { abi, evm } = require("./compile");

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log("Attempting to deploy from account ", accounts[0]);

  const result = await new web3.eth.Contract(abi)
    .deploy({
      data: evm.bytecode.object,
      arguments: ["Hi there"],
    })
    .send({ from: accounts[0], gas: 1000000 });

  console.log("Contract deployed to ", result?.options?.address);

  provider.engine.stop();
};
deploy();
